<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 25.03.2018
 * Time: 10:17
 */

class Task
{
    const SHOW_BY_DEFAULT = 3;

    public static function getTaskItemById($id)
    {
        $id = intval($id);
        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM taskman WHERE id =' .$id);
            $result ->setFetchMode(PDO::FETCH_ASSOC);

            $taskItem = $result->fetch();

            return $taskItem;
        }
    }

    //Returns an array of tasks
    public static function getTaskListBySort($sort, $page = 1)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();
        $taskList = [];

        $result = $db->query('SELECT id, name, email, task, image FROM taskman '
            . 'ORDER BY '. $sort .' DESC LIMIT ' . self::SHOW_BY_DEFAULT . ' OFFSET ' . $offset);

        $i = 0;
        while ($row = $result->fetch()) {
            $taskList[$i]['id'] = $row['id'];
            $taskList[$i]['name'] = $row['name'];
            $taskList[$i]['email'] = $row['email'];
            $taskList[$i]['task'] = $row['task'];
            $taskList[$i]['image'] = $row['image'];
            $i++;
        }
        return $taskList;

    }

    //Returns total products
    public static function getTotalTasks()
    {
        $db = Db::getConnection();

        $result = $db->prepare('SELECT count(id) AS count FROM taskman WHERE id > "0" ');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];
    }
}