<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Последние задачи</h2>
                    
                    <?php foreach ($taskList as $task): ?>
                        <div>
                            <p>
                                <span class="-task-"></span>
                                Задача № <?php echo $task['id'];?>. Автор: <?php echo $task['name'];?>.<br>
                                <?php echo $task['task'];?>
                            </p>
                        </div>
                    <?php endforeach;?>
                    <!-- Постраничная навигация -->
                    <?php echo $pagination->get(); ?>
                </div><!--features_items-->

            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>