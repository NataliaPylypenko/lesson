<?php

return [
    
    'task/([0-9]+)' => 'task/view/$1', // actionView в TaskController

    'task/page-([0-9+])' => 'task/index/$1', // actionIndex в TaskController

    '' => 'task/index', // actionIndex в TaskController
    
];
