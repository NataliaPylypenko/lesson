<?php
require_once ROOT.'/models/Task.php';
require_once ROOT.'/components/Pagination.php';
class TaskController
{

    public function actionIndex()
    {
        $sort = 'id';
        //Список задач
        if (isset($_POST['sort'])) {
            switch ($_POST['sort']) {
                case '1':
                    $sort = 'name';
                    break;
                case '2':
                    $sort = 'email';
                    break;
                case '3':
                    $sort = 'status';
                    break;
            }
        }

        $taskList = [];
        $taskList = Task::getTaskListBySort($sort, $page = 1);

        $total = Task::getTotalTasks();

        // Создаем объект Pagination - постраничная навигация
        $pagination = new Pagination($total, $page, Task::SHOW_BY_DEFAULT, 'page-');

        require_once(ROOT . '/views/site/index.php');

        return true;
    }

    public function actionView($id)
    {
        //Просмотр одной задачи
        if ($id) {
            $taskItem = Task::getTaskItemById($id);
        }

        return true;
    }

       /* public function actionPagination($page = 1)
        {
            $total = Task::getTotalTasks();

            // Создаем объект Pagination - постраничная навигация
            $pagination = new Pagination($total, $page, Task::SHOW_BY_DEFAULT, 'page-');

            require_once(ROOT . '/views/site/index.php');

            return true;
        }*/
}
